/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.overloadingmethod;

/**
 *
 * @author Ow
 */
public class Drinks {
    private String drinks;
    private int quantity;
    
    //Overloading
    
    public void buydrinks() {
        System.out.println("You got 1 bottle of water");
    }
    
    public void buydrinks(int quantity) {
        System.out.println("You got " + quantity + " bottle of water");
    }
    
    public void buydrinks(String drinks) {
        System.out.println("You got 1 of " + drinks);
    }
    
    public void buydrinks(String drinks,int quantity) {
        System.out.println("You got " + quantity + " of " + drinks);
    }

}
